https://developers.redhat.com/products/rhel/download/
https://developers.redhat.com/products/rhel/hello-world/

https://www.softwarecollections.org/en/scls/rhscl/devtoolset-6/

https://rpmfind.net/linux/RPM/fedora/devel/rawhide/x86_64/b/boost-1.63.0-9.fc27.i686.html

subscription-manager register
subscription-manager attach
subscription-manager refresh
subscription-manager refresh
yum list updates

# On RHEL, enable RHSCL repository for you system:
$ sudo yum-config-manager --enable rhel-server-rhscl-7-rpms

# 2. Install the collection:
$ sudo yum install devtoolset-6

# 3. Start using software collections:
$ scl enable devtoolset-6 bash


# EPEL
subscription-manager repos --enable rhel-7-server-extras-rpms
subscription-manager repos --enable rhel-7-server-optional-rpms
rpm -Uvh http://download.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-9.noarch.rpm

# boost
rpm -Uvh --nodeps ftp://fr2.rpmfind.net/linux/fedora/linux/development/rawhide/Everything/x86_64/os/Packages/b/boost-1.63.0-9.fc27.x86_64.rpm



