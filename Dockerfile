FROM centos:7.3.1611
MAINTAINER Evren KANALICI <evren.kanalici [at] nevalabs [dot] com>

LABEL version="1.0"
LABEL description="Core-dev environment with boost, opencv and premake"

RUN yum list updates
RUN yum -y install centos-release-scl
RUN yum -y install devtoolset-6
RUN scl enable devtoolset-6 bash

RUN yum -y install bzip2 cmake git svn

# RUN apk add --no-cache --virtual .build-deps g++ make curl linux-headers python-dev

RUN cd /tmp \
    && curl -SL "http://downloads.sourceforge.net/project/boost/boost/1.63.0/boost_1_63_0.tar.bz2" -o boost_1_63_0.tar.bz2

ENV OPENCV_VERS 3.2.0
RUN cd /tmp \
    && curl -SL -o opencv.zip https://github.com/opencv/opencv/archive/${OPENCV_VERS}.zip \
    && curl -SL -o opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/${OPENCV_VERS}.zip

ENV PATH $PATH:/opt/rh/devtoolset-6/root/usr/bin

RUN cd /tmp \
    && unzip opencv.zip >/dev/null 2>&1 && mv opencv-${OPENCV_VERS} opencv \
    && unzip opencv_contrib.zip >/dev/null 2>&1 && mv opencv_contrib-${OPENCV_VERS} opencv_contrib \
    && cd opencv \
    && mkdir -p release && cd release \
    && cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D OPENCV_EXTRA_MODULES_PATH=/tmp/opencv_contrib/modules ..

RUN cd /tmp \
    && [ $(sha1sum boost_1_63_0.tar.bz2 | awk '{print $1}') == '9f1dd4fa364a3e3156a77dc17aa562ef06404ff6' ] \
    && tar --bzip2 -xf boost_1_63_0.tar.bz2 \
    && cd boost_1_63_0 \
    && ./bootstrap.sh --prefix=/usr/local --without-libraries=python

RUN cd /tmp/boost_1_63_0 \
    && ./b2 -a -sHAVE_ICU=1 -j4 --build-type=complete --layout=versioned address-model=64 cxxflags="-fPIC -std=c++14" linkflags="-fPIC" \
    && ./b2 install \
    && rm -rf /tmp/boost*

RUN cd /tmp/opencv/release \
    && make -j4 \
    && make install \
    && rm -rf /tmp/opencv*

# install git-lfs
RUN cd /tmp \
    && curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | bash \
    && yum -y install git-lfs \
    && git lfs install >/dev/null 2>&1

RUN cd /tmp \
    && git clone https://github.com/premake/premake-core.git \
    && cd premake-core \
    && git checkout tags/v5.0.0-alpha11 \
    && git submodule update --init \
    && make -f Bootstrap.mak linux >/dev/null 2>&1 \
    && cp bin/release/premake5 /usr/local/bin \
    && rm -rf /tmp/premake*

RUN yum clean all

# User env. variables
ENV DEV_ENV_LONG      "centos-7.3"
ENV DEV_ENV_SHORT     "centos"

ENTRYPOINT /bin/bash


